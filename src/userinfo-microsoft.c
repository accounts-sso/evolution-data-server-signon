/*
 * userinfo-microsoft.c
 *
 * This library is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "userinfo-microsoft.h"

#include <glib/gi18n-lib.h>
#include <rest/rest-proxy.h>
#include <json-glib/json-glib.h>
#include <libsignon-glib/signon-glib.h>

#define WINDOWS_LIVE_ME_URI \
	"https://apis.live.net/v5.0/me"

typedef struct _AsyncContext AsyncContext;

struct _AsyncContext {
	gchar *user_identity;
	gchar *email_address;
};

static void
async_context_free (AsyncContext *async_context)
{
	g_free (async_context->user_identity);
	g_free (async_context->email_address);

	g_slice_free (AsyncContext, async_context);
}

static void
e_ag_account_microsoft_got_me_cb (RestProxyCall *call,
                                     const GError *error,
                                     GObject *weak_object,
                                     gpointer user_data)
{
	GTask *task;
	JsonParser *json_parser;
	JsonObject *json_object;
	JsonNode *json_node;
	const gchar *json_string;
	gchar *id;
	gchar *email;
	GError *local_error = NULL;

	task = G_TASK (user_data);

	if (error != NULL) {
		g_task_return_error (task, g_error_copy (error));
		g_object_unref (task);
		return;
	}

	/* This is shamelessly stolen from goawindowsliveprovider.c */

	if (rest_proxy_call_get_status_code (call) != 200) {
		g_task_return_new_error (
			task, G_IO_ERROR, G_IO_ERROR_FAILED,
			_("Expected status 200 when requesting your "
			"identity, instead got status %d (%s)"),
			rest_proxy_call_get_status_code (call),
			rest_proxy_call_get_status_message (call));
		g_object_unref (task);
		return;
	}

	json_parser = json_parser_new ();
	json_parser_load_from_data (
		json_parser,
		rest_proxy_call_get_payload (call),
		rest_proxy_call_get_payload_length (call),
		&local_error);

	if (local_error != NULL) {
		g_prefix_error (
			&local_error,
			_("Error parsing response as JSON: "));
		g_task_return_error (task, local_error);
		g_object_unref (json_parser);
		g_object_unref (task);
		return;
	}

	json_node = json_parser_get_root (json_parser);
	json_object = json_node_get_object (json_node);
	json_string = json_object_get_string_member (json_object, "id");
	id = g_strdup (json_string);

	json_object = json_object_get_object_member (json_object, "emails");
	json_string = json_object_get_string_member (json_object, "account");
	email = g_strdup (json_string);

	if (id == NULL) {
		g_task_return_new_error (
			task, G_IO_ERROR, G_IO_ERROR_FAILED,
			_("Didn’t find “id” in JSON data"));
		g_free (email);
	} else if (email == NULL) {
		g_task_return_new_error (
			task, G_IO_ERROR, G_IO_ERROR_FAILED,
			_("Didn’t find “emails.account” in JSON data"));
	} else {
		AsyncContext *async_context = g_slice_new0 (AsyncContext);
		async_context->user_identity = id;
		async_context->email_address = email;
		g_task_return_pointer (task, async_context,
			(GDestroyNotify) async_context_free);
	}

	g_object_unref (json_parser);
	g_object_unref (task);
}

static void
e_ag_account_microsoft_session_process_cb (GObject *source_object,
                                              GAsyncResult *result,
                                              gpointer user_data)
{
	GTask *task;
	GVariant *session_data;
	GError *local_error = NULL;

	task = G_TASK (user_data);

	session_data = signon_auth_session_process_finish (
		SIGNON_AUTH_SESSION (source_object), result, &local_error);

	/* Sanity check. */
	g_warn_if_fail (
		((session_data != NULL) && (local_error == NULL)) ||
		((session_data == NULL) && (local_error != NULL)));

	/* Use the access token to obtain the user's email address. */

	if (session_data != NULL) {
		RestProxy *proxy;
		RestProxyCall *call;
		gchar *access_token = NULL;

		g_variant_lookup (
			session_data, "AccessToken", "s", &access_token);

		g_variant_unref (session_data);

		proxy = rest_proxy_new (WINDOWS_LIVE_ME_URI, FALSE);
		call = rest_proxy_new_call (proxy);
		rest_proxy_call_set_method (call, "GET");

		/* XXX This should never be NULL, but if it is just let
		 *     the call fail and pick up the resulting GError. */
		if (access_token != NULL) {
			rest_proxy_call_add_param (
				call, "access_token", access_token);
			g_free (access_token);
		}

		rest_proxy_call_async (
			call, e_ag_account_microsoft_got_me_cb,
			NULL, task, &local_error);

		g_object_unref (proxy);
		g_object_unref (call);
	} else {
		g_task_return_error (task, local_error);
		g_object_unref (task);
	}

}

void
e_ag_account_collect_microsoft_userinfo (GTask *task)
{
	AgAccount *ag_account = g_task_get_source_object (task);
	AgAccountService *ag_account_service = NULL;
	SignonAuthSession *session;
	AgAuthData *ag_auth_data;
	GList *list;
	GError *local_error = NULL;

	/* First obtain an OAuth 2.0 access token. */

	list = ag_account_list_services_by_type (ag_account, "mail");
	if (list != NULL) {
		ag_account_service = ag_account_service_new (
			ag_account, (AgService *) list->data);
		ag_service_list_free (list);
	}

	g_warn_if_fail (ag_account_service != NULL);

	ag_auth_data = ag_account_service_get_auth_data (ag_account_service);

	session = signon_auth_session_new (
		ag_auth_data_get_credentials_id (ag_auth_data),
		ag_auth_data_get_method (ag_auth_data), &local_error);

	/* Sanity check. */
	g_warn_if_fail (
		((session != NULL) && (local_error == NULL)) ||
		((session == NULL) && (local_error != NULL)));

	if (session != NULL) {
		signon_auth_session_process (
			session,
			ag_auth_data_get_login_parameters (ag_auth_data, NULL),
			ag_auth_data_get_mechanism (ag_auth_data),
			g_task_get_cancellable (task),
			e_ag_account_microsoft_session_process_cb,
			task);
	} else {
		g_task_return_error (task, local_error);
		g_object_unref (task);
	}

	ag_auth_data_unref (ag_auth_data);

	g_object_unref (ag_account_service);
}
