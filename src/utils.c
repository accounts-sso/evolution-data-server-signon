/*
 * utils.c
 *
 * This library is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <glib/gi18n-lib.h>
#include <rest/rest-proxy.h>
#include <json-glib/json-glib.h>
#include <libsignon-glib/signon-glib.h>

#include "utils.h"
#include "userinfo-google.h"
#include "userinfo-microsoft.h"
#include "userinfo-yahoo.h"

typedef struct _AsyncContext AsyncContext;

struct _AsyncContext {
	gchar *user_identity;
	gchar *email_address;
};

static void
async_context_free (AsyncContext *async_context)
{
	g_clear_pointer (&async_context->user_identity, g_free);
	g_clear_pointer (&async_context->email_address, g_free);

	g_slice_free (AsyncContext, async_context);
}

void
e_ag_account_collect_userinfo (AgAccount *ag_account,
                               GCancellable *cancellable,
                               GAsyncReadyCallback callback,
                               gpointer user_data)
{
	g_autoptr(GTask) task = NULL;
	const gchar *backend_name;

	g_return_if_fail (AG_IS_ACCOUNT (ag_account));

	backend_name = signon_get_backend_name (ag_account);
	g_return_if_fail (backend_name != NULL);

	task = g_task_new (ag_account, cancellable, callback, user_data);
	g_task_set_source_tag (task, e_ag_account_collect_userinfo);

	/* XXX This has to be done differently for each provider. */

	if (g_strcmp0 (backend_name, "google") == 0) {
		e_ag_account_collect_google_userinfo (g_steal_pointer (&task));
	} else if (g_strcmp0 (backend_name, "outlook") == 0) {
		e_ag_account_collect_microsoft_userinfo (g_steal_pointer (&task));
	} else if (g_strcmp0 (backend_name, "yahoo") == 0) {
		e_ag_account_collect_yahoo_userinfo (g_steal_pointer (&task));
	} else {
		g_warn_if_reached ();
		g_task_return_pointer (task, NULL, NULL);
	}
}

gboolean
e_ag_account_collect_userinfo_finish (AgAccount *ag_account,
                                      GAsyncResult *result,
                                      gchar **out_user_identity,
                                      gchar **out_email_address,
                                      gchar **out_imap_user_name,
                                      gchar **out_smtp_user_name,
                                      GError **error)
{
	AsyncContext *async_context;

	g_return_val_if_fail (g_task_is_valid (result, ag_account), FALSE);

	async_context = g_task_propagate_pointer (G_TASK (result), error);

	if (async_context == NULL)
		return FALSE;

	/* The result strings may be NULL without an error. */

	if (out_user_identity != NULL) {
		*out_user_identity = g_steal_pointer (&async_context->user_identity);
	}

	if (out_email_address != NULL) {
		*out_email_address = g_strdup (async_context->email_address);
	}

	if (out_imap_user_name != NULL) {
		*out_imap_user_name = g_strdup (async_context->email_address);
	}

	if (out_smtp_user_name != NULL) {
		*out_smtp_user_name = g_steal_pointer (&async_context->email_address);
	}

	async_context_free (async_context);
	return TRUE;
}

const gchar *
e_source_get_ag_service_type (ESource *source)
{
	const gchar *extension_name;

	g_return_val_if_fail (E_IS_SOURCE (source), NULL);

	/* Determine an appropriate service type based on
	 * which extensions are present in the ESource. */

	extension_name = E_SOURCE_EXTENSION_ADDRESS_BOOK;
	if (e_source_has_extension (source, extension_name))
		return E_AG_SERVICE_TYPE_CONTACTS;

	extension_name = E_SOURCE_EXTENSION_CALENDAR;
	if (e_source_has_extension (source, extension_name))
		return E_AG_SERVICE_TYPE_CALENDAR;

	extension_name = E_SOURCE_EXTENSION_MEMO_LIST;
	if (e_source_has_extension (source, extension_name))
		return E_AG_SERVICE_TYPE_CALENDAR;

	extension_name = E_SOURCE_EXTENSION_TASK_LIST;
	if (e_source_has_extension (source, extension_name))
		return E_AG_SERVICE_TYPE_CALENDAR;

	extension_name = E_SOURCE_EXTENSION_MAIL_ACCOUNT;
	if (e_source_has_extension (source, extension_name))
		return E_AG_SERVICE_TYPE_MAIL;

	extension_name = E_SOURCE_EXTENSION_MAIL_TRANSPORT;
	if (e_source_has_extension (source, extension_name))
		return E_AG_SERVICE_TYPE_MAIL;

	g_return_val_if_reached (NULL);
}

const gchar *
signon_get_backend_name (AgAccount *account)
{
	GVariant *backend_variant = NULL;

	g_return_val_if_fail (AG_IS_ACCOUNT (account), NULL);

	backend_variant = ag_account_get_variant (account, "evolution-data-server/backend", NULL);
	if (backend_variant) {
		return g_variant_get_string (backend_variant, NULL);
	}

	return NULL;
}
