/*
 * userinfo-google.h
 *
 * This library is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __USERINFO_GOOGLE_H__
#define __USERINFO_GOOGLE_H__

#include "utils.h"

G_BEGIN_DECLS

void e_ag_account_collect_google_userinfo (GTask *task);

G_END_DECLS

#endif /* __USERINFO_GOOGLE_H__ */
