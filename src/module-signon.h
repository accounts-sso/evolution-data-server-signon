/*
 * module-signon.h
 *
 * This library is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __MODULE_SIGNON_H__
#define __MODULE_SIGNON_H__

#include <glib/gi18n-lib.h>
#include <libsignon-glib/signon-glib.h>
#include <libaccounts-glib/accounts-glib.h>

/* Standard GObject macros */
#define E_TYPE_SIGNON (e_signon_get_type ())
#define E_SIGNON(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), E_TYPE_SIGNON, ESignon))

#define CAMEL_OAUTH2_MECHANISM_NAME "XOAUTH2"

G_BEGIN_DECLS

typedef struct _ESignon ESignon;
typedef struct _ESignonClass ESignonClass;
typedef struct _AsyncContext AsyncContext;

/* Module Entry Points */
void e_module_load (GTypeModule *type_module);
void e_module_unload (GTypeModule *type_module);

GType e_signon_get_type (void);

G_END_DECLS

#endif /* __MODULE_SIGNON_H__ */
