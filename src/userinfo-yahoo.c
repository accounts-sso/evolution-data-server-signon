/*
 * userinfo-yahoo.c
 *
 * This library is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "userinfo-yahoo.h"

#include <glib/gi18n-lib.h>
#include <rest/rest-proxy.h>
#include <json-glib/json-glib.h>
#include <libsignon-glib/signon-glib.h>

typedef struct _AsyncContext AsyncContext;

struct _AsyncContext {
	gchar *user_identity;
	gchar *email_address;
};

static void
async_context_free (AsyncContext *async_context)
{
	g_free (async_context->user_identity);
	g_free (async_context->email_address);

	g_slice_free (AsyncContext, async_context);
}

void
e_ag_account_collect_yahoo_userinfo (GTask *task)
{
	AgAccount *ag_account = g_task_get_source_object (task);
	AsyncContext *async_context;
	GString *email_address;
	const gchar *display_name;

	/* XXX This is a bit of a hack just to get *something* working
	 *     for Yahoo! accounts.  The proper way to obtain userinfo
	 *     for Yahoo! is through OAuth 1.0 and OpenID APIs, which
	 *     does not look trivial.  This will do for now. */

	/* XXX AgAccount's display name also sort of doubles as a user
	 *     name, which may or may not be an email address.  If the
	 *     display name has no domain part, assume "@yahoo.com". */

	display_name = ag_account_get_display_name (ag_account);
	g_warn_if_fail (display_name != NULL);

	email_address = g_string_new (display_name);
	if (strchr (email_address->str, '@') == NULL)
		g_string_append (email_address, "@yahoo.com");

	async_context = g_slice_new0 (AsyncContext);
	async_context->user_identity = g_strdup (email_address->str);
	async_context->email_address = g_strdup (email_address->str);

	g_task_return_pointer (task, async_context,
		(GDestroyNotify) async_context_free);

	g_object_unref (task);
}
