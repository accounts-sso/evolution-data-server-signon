Evolution Data Server support
=============================

Evolution Data Server support for the signon library.

This project contains:
 - the crendentials-module that is a ESourceCredentialsProviderImpl implementation
 - the signon module that is a EExtension implementation

Build instructions
------------------

This project depends on GLib and Evolution Data Server, and uses the Meson build system. To build it, run
```
meson build --prefix=/usr
cd build
ninja
sudo ninja install
```

License
-------

See COPYING file.
